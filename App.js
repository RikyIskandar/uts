import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Nav from './Nav';

const Header = () => {
  return (
    <View style={styles.kotak}>
      <Text style={styles.teks}>Kamus Hewan</Text>
      <Nav />
    </View>
  );
};

const App = () => {
  return (
    <View style={styles.kotak2}>
      <Header />
      <Text>Functional Component</Text>
    </View>
  );
};

export default App;

const styles = StyleSheet.create({
  kotak:{
    backgroundColor : 'red',
    padding : 10,
  },
  teks:{
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
  kotak2:{
    backgroundColor: '#00fa9a',
    margin: 5,
    padding: 10,
  }
});
